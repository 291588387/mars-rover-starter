package com.afs.tdd;

public enum Direction {
    North,
    South,
    East,
    West
}
