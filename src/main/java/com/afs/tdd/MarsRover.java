package com.afs.tdd;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MarsRover {
    private final Location location;
    private Map<Direction, Direction> turnLeftRule = new HashMap();
    private Map<Direction, Direction> turnRightRule = new HashMap();
    public MarsRover(Location location) {
        this.location = location;
        turnLeftRule.put(Direction.North, Direction.West);
        turnLeftRule.put(Direction.West, Direction.South);
        turnLeftRule.put(Direction.South, Direction.East);
        turnLeftRule.put(Direction.East, Direction.North);
        turnRightRule.put(Direction.North, Direction.East);
        turnRightRule.put(Direction.East, Direction.South);
        turnRightRule.put(Direction.South, Direction.West);
        turnRightRule.put(Direction.West, Direction.North);
    }

    public Location getLocation() {
        return location;
    }

    public void executeCommand(Command command) {
        if (command.equals(Command.Move)) {
            move();
        } else if (command.equals(Command.Left)) {
            turnLeft();
        } else if (command.equals(Command.Right)) {
            turnRight();
        }
    }

    public void executeCommandBatch(List<Command> commandList) {
        commandList.forEach(this::executeCommand);
    }

    private void turnRight() {
        location.setDirection(turnRightRule.get(location.getDirection()));
    }

    private void turnLeft() {
        location.setDirection(turnLeftRule.get(location.getDirection()));
    }

    private void move() {
        if (location.getDirection().equals(Direction.North)) {
            location.setCoordinateY(location.getCoordinateY() + 1);
        } else if (location.getDirection().equals(Direction.East)) {
            location.setCoordinateX(location.getCoordinateX() + 1);
        } else if (location.getDirection().equals(Direction.South)) {
            location.setCoordinateY(location.getCoordinateY() - 1);
        } else if (location.getDirection().equals(Direction.West)) {
            location.setCoordinateX(location.getCoordinateX() - 1);
        }
    }
}
