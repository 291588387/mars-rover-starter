package com.afs.tdd;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MarsRoverTest {
    @Test
    void should_change_to_0_1_N_when_executeCommand_given_location_0_0_N_and_command_M()
    {
        //given
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Move);
        //then
        assertEquals(0 , marsRover.getLocation().getCoordinateX());
        assertEquals(1 , marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.North , marsRover.getLocation().getDirection());
    }
    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_0_0_N_and_command_L()
    {
        //given
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Left);
        //then
        assertEquals(0 , marsRover.getLocation().getCoordinateX());
        assertEquals(0 , marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.West , marsRover.getLocation().getDirection());
    }
    @Test
    void should_change_to_0_0_E_when_executeCommand_given_location_0_0_N_and_command_R()
    {
        //given
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Right);
        //then
        assertEquals(0 , marsRover.getLocation().getCoordinateX());
        assertEquals(0 , marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.East , marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_to_1_0_E_when_executeCommand_given_location_0_0_E_and_command_M()
    {
        //given
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Move);
        //then
        assertEquals(1 , marsRover.getLocation().getCoordinateX());
        assertEquals(0 , marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.East , marsRover.getLocation().getDirection());
    }
    @Test
    void should_change_to_0_0_N_when_executeCommand_given_location_0_0_E_and_command_L()
    {
        //given
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Left);
        //then
        assertEquals(0 , marsRover.getLocation().getCoordinateX());
        assertEquals(0 , marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.North , marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_0_E_and_command_R()
    {
        //given
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Right);
        //then
        assertEquals(0 , marsRover.getLocation().getCoordinateX());
        assertEquals(0 , marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.South , marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_1_S_and_command_M()
    {
        //given
        Location location = new Location(0,1,Direction.South);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Move);
        //then
        assertEquals(0 , marsRover.getLocation().getCoordinateX());
        assertEquals(0 , marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.South , marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_to_0_0_E_when_executeCommand_given_location_0_0_S_and_command_L()
    {
        //given
        Location location = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Left);
        //then
        assertEquals(0 , marsRover.getLocation().getCoordinateX());
        assertEquals(0 , marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.East , marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_0_0_S_and_command_R()
    {
        //given
        Location location = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Right);
        //then
        assertEquals(0 , marsRover.getLocation().getCoordinateX());
        assertEquals(0 , marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.West , marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_1_0_W_and_command_M()
    {
        //given
        Location location = new Location(1,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Move);
        //then
        assertEquals(0 , marsRover.getLocation().getCoordinateX());
        assertEquals(0 , marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.West , marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_0_W_and_command_L()
    {
        //given
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Left);
        //then
        assertEquals(0 , marsRover.getLocation().getCoordinateX());
        assertEquals(0 , marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.South , marsRover.getLocation().getDirection());
    }
    @Test
    void should_change_to_0_0_N_when_executeCommand_given_location_0_0_W_and_command_R()
    {
        //given
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);
        //when
        marsRover.executeCommand(Command.Right);
        //then
        assertEquals(0 , marsRover.getLocation().getCoordinateX());
        assertEquals(0 , marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.North , marsRover.getLocation().getDirection());
    }

    @Test
    void should_change_to_1_1_N_when_executeCommand_given_location_0_0_N_and_command_MRML()
    {
        //given
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);
        List<Command> commandList = List.of(Command.Move, Command.Right, Command.Move, Command.Left);
        //when
        marsRover.executeCommandBatch(commandList);
        //then
        assertEquals(1 , marsRover.getLocation().getCoordinateX());
        assertEquals(1 , marsRover.getLocation().getCoordinateY());
        assertEquals(Direction.North , marsRover.getLocation().getDirection());
    }
}
