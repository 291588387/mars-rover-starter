# ORID
## O:
- Review homework code in the morning, recorded and fixed the problems we found in time. Get to know the implementation of **Observer Mode** in our homework code.
- Introduced **Unit Testing**, got to know the concept and importance of it. By following Given, When, Then steps we practiced unitesting with an actual demo, using **JUnit 5**.
- Contacted Test Driven Development, focus on writting tests or writting implementation one at a time. The loop goes like : ```fail test -> implementation -> refactor```
## R:

- I felt quite familiar with unit testing and longing to make progress as well.
## I:

- I felt familiar since I've done similar jobs back when I was an intern of EUC team,  so such lesson helps a lot to solid the foundation for better improvement.
- I was well benefited after following the fail test -> implementation -> refactor procedure in actual coding scene, since I received quick feedback during coding period.

## D:

- Follow the TDD procedure strictly and form good developing habits like commit every implementation in baby small steps, as well as refactoring the code in a certain session.